﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddinMailCheck
{


    // Event handler used to correctly clean up resources.
    // <param name="id">The unique id of the Inspector instance.</param>
    internal delegate void InspectorWrapperClosedEventHandler(Guid id);

    // The base class for all inspector wrappers.
    internal abstract class InspectorWrapper
    {

        // Event notification for the InspectorWrapper.Closed event.
        // This event is raised when an inspector has been closed.
        public event InspectorWrapperClosedEventHandler Closed;

        // The unique ID that identifies the inspector window.
        public Guid Id { get; private set; }

        // The Outlook Inspector instance.
        public Microsoft.Office.Interop.Outlook.Inspector Inspector { get; private set; }

        // .ctor
        // <param name="inspector">The Outlook Inspector instance that should be handled.</param>
        public InspectorWrapper(Microsoft.Office.Interop.Outlook.Inspector inspector)
        {
            Id = Guid.NewGuid();
            Inspector = inspector;
            // Register Inspector events here
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).Close +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_CloseEventHandler(Inspector_Close);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).Activate +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_ActivateEventHandler(Activate);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).Deactivate +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_DeactivateEventHandler(Deactivate);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeMaximize +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeMaximizeEventHandler(BeforeMaximize);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeMinimize +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeMinimizeEventHandler(BeforeMinimize);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeMove +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeMoveEventHandler(BeforeMove);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeSize +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeSizeEventHandler(BeforeSize);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).PageChange +=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_PageChangeEventHandler(PageChange);

            // Initialize is called to give the derived wrappers.
            Initialize();
        }
        // Event handler for the Inspector Close event.
        private void Inspector_Close()
        {
            // Call the Close method - the derived classes can implement cleanup code
            // by overriding the Close method.
            Close();
            // Unregister Inspector events.
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).Close -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_CloseEventHandler(Inspector_Close);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).Activate -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_ActivateEventHandler(Activate);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).Deactivate -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_DeactivateEventHandler(Deactivate);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeMaximize -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeMaximizeEventHandler(BeforeMaximize);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeMinimize -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeMinimizeEventHandler(BeforeMinimize);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeMove -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeMoveEventHandler(BeforeMove);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).BeforeSize -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_BeforeSizeEventHandler(BeforeSize);
            ((Microsoft.Office.Interop.Outlook.InspectorEvents_10_Event)Inspector).PageChange -=
                new Microsoft.Office.Interop.Outlook.InspectorEvents_10_PageChangeEventHandler(PageChange);
            // Clean up resources and do a GC.Collect().
            Inspector = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            // Raise the Close event.
            if (Closed != null) Closed(Id);
        }

        protected virtual void Initialize() { }

        // Method is called when another page of the inspector has been selected.
        // <param name="ActivePageName">The active page name by reference.</param>
        protected virtual void PageChange(ref string ActivePageName) { }

        // Method is called before the inspector is resized.
        // <param name="Cancel">To prevent resizing, set Cancel to true.</param>
        protected virtual void BeforeSize(ref bool Cancel) { }

        // Method is called before the inspector is moved around.
        // <param name="Cancel">To prevent moving, set Cancel to true.</param>
        protected virtual void BeforeMove(ref bool Cancel) { }

        // Method is called before the inspector is minimized.
        // <param name="Cancel">To prevent minimizing, set Cancel to true.</param>
        protected virtual void BeforeMinimize(ref bool Cancel) { }

        // Method is called before the inspector is maximized.
        // <param name="Cancel">To prevent maximizing, set Cancel to true.</param>
        protected virtual void BeforeMaximize(ref bool Cancel) { }

        // Method is called when the inspector is deactivated.
        protected virtual void Deactivate() { }

        // Method is called when the inspector is activated.
        protected virtual void Activate() { }

        // Derived classes can do a cleanup by overriding this method.
        protected virtual void Close() { }

        public static InspectorWrapper GetWrapperFor(Microsoft.Office.Interop.Outlook.Inspector inspector)
        {

            string messageClass = inspector.CurrentItem.GetType().InvokeMember("MessageClass", BindingFlags.GetProperty, null, inspector.CurrentItem, null);
            switch (messageClass)
            {
                case "IPM.Note":
                    return new MailItemWrapper(inspector);
            }
            return null;
        }

    }


}
