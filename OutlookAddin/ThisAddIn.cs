﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Diagnostics;
using System.Windows.Forms;
using System.Reflection;
using OutlookAddin;
using System.Threading.Tasks;

namespace OutlookAddinMailCheck
{
    public partial class ThisAddIn
    {

        private Outlook.Explorer test;
        private Outlook.MailItem mailItem;
        private Outlook.Inspectors _inspectors;
        private Dictionary<Guid, InspectorWrapper> _wrappedInspectors;

        private string _panelName = "SlideBar";
        private Panel task;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            test = Application.ActiveExplorer();
            test.SelectionChange += Test_SelectionChange;
            test.BeforeFolderSwitch += Test_BeforeFolderSwitch;

            this.Application.ItemLoad += Application_ItemLoad;
            _wrappedInspectors = new Dictionary<Guid, InspectorWrapper>();
            _inspectors = Globals.ThisAddIn.Application.Inspectors;
            _inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(WrapInspector);
            Application.ItemSend += Application_ItemSend;
         
        }

        private void Application_ItemSend(object Item, ref bool Cancel)
        {
             string postAttemptUrl = "http://dev.marketsgroup.org/experiments/blacklist_logger";
             string postAllAtempsUrl = "http://dev.marketsgroup.org/experiments/all_emails_logger";

        mailItem = Item as Outlook.MailItem;
            if (mailItem != null)
            {
                Job job = new Job();
                var listOfEmails =  Task.Run(() => job.CreatGetReq());
                List<Emails> blackList = listOfEmails.Result.ToList();

                foreach (Emails item in blackList) {
                    if (mailItem.To.Contains(item.email))
                    {
                        MessageBox.Show("You may not send this message - the recipient is Blacklisted.");
                        Cancel = true;
                        Task.Run(() => job.CreatPostReq(postAttemptUrl, mailItem.SendUsingAccount.ToString(), item.email, mailItem.Subject));
                    }
                    if (mailItem.CC.Contains(item.email))
                    {
                        MessageBox.Show("You may not send this message - the recipient is Blacklisted.");
                        Cancel = true;
                        Task.Run(() => job.CreatPostReq(postAttemptUrl, mailItem.SendUsingAccount.ToString(), item.email, mailItem.Subject));

                    }
                    if (mailItem.BCC.Contains(item.email))
                    {
                        MessageBox.Show("You may not send this message - the recipient is Blacklisted.");
                        Cancel = true;
                        Task.Run(() => job.CreatPostReq(postAttemptUrl, mailItem.SendUsingAccount.ToString(), item.email, mailItem.Subject));
                    }

                }
                Task.Run(() => job.CreatPostReq(postAllAtempsUrl, mailItem.SendUsingAccount.ToString(), mailItem.To, mailItem.Subject));
            }
        }

        private void Test_BeforeFolderSwitch(object NewFolder, ref bool Cancel)
        {
          
        }

        private void Test_SelectionChange()
        {
            foreach (object selection in this.Application.ActiveExplorer().Selection)
            {
                mailItem = selection as Outlook.MailItem;
                if (mailItem != null)
                {
                  
                }
            }

    }
        private void Application_ItemLoad(object Item)
        {
            mailItem = Item as Outlook.MailItem;
            if (mailItem != null)
            {

            }
        }

        void WrapInspector(Outlook.Inspector inspector)
        {
            InspectorWrapper wrapper = InspectorWrapper.GetWrapperFor(inspector);
            if (wrapper != null)
            {
                wrapper.Closed += new InspectorWrapperClosedEventHandler(wrapper_Closed);
                _wrappedInspectors[wrapper.Id] = wrapper;
            }
        }

        void wrapper_Closed(Guid id)
        {
            _wrappedInspectors.Remove(id);
        }

       

      
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO generated coed
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
