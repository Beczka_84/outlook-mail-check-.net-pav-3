﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddinMailCheck
{
    internal class MailItemWrapper : InspectorWrapper
    {

        public MailItemWrapper(Microsoft.Office.Interop.Outlook.Inspector inspector)
            : base(inspector)
        {
        }

        public Microsoft.Office.Interop.Outlook.MailItem Item { get; private set; }

        protected override void Initialize()
        {
            Item = (Microsoft.Office.Interop.Outlook.MailItem)Inspector.CurrentItem;
            if (Inspector != null)
            {
                ((InspectorEvents_10_Event)Inspector).Close += Inspector_Close;
            }
            Item.Read += Item_Read;
  

        }

        private void Item_Read()
        {
          
        }

        private void Inspector_Close()
        {

        }



        protected override void Close()
        {
            Item.Read -= Item_Read;
            Item = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
