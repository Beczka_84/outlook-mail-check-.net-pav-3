﻿using Newtonsoft.Json;
using OutlookAddin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddinMailCheck
{
    public class Job
    {
       
        private string getAllEmailsUrl = "http://dev.marketsgroup.org/experiments/blacklist_show";

        public async void CreatPostReq(string url, string to, string from, string subject)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            string postData = "to=" + to + "&from=" + from + "&subject=" + subject + "";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            WebResponse response = await request.GetResponseAsync();
            var x = ((HttpWebResponse)response).StatusCode;

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseFromServer = reader.ReadToEnd();
          
               
        }

        public async Task<List<Emails>> CreatGetReq()
        {
            List<Emails> emails = new List<Emails>();

            WebRequest request = WebRequest.Create(getAllEmailsUrl);
            request.Method = "GET";

            WebResponse response = await request.GetResponseAsync();
            var statusCode = ((HttpWebResponse)response).StatusCode;

            if (statusCode == HttpStatusCode.OK)
            { 
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string responseFromServer = reader.ReadToEnd();
                Debug.WriteLine(responseFromServer);

                emails = JsonConvert.DeserializeObject<List<Emails>>(responseFromServer);
                return emails;
            }

            return emails;
        }
    }
}
